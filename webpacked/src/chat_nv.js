import './scss/styles.scss'
import {Chat} from './chat/Chat'

(function ($, Drupal, drupalSettings) {

  $(document).ready(function () {
    const messages_url = drupalSettings.dru_chat.msgs_url
    const new_message_url = drupalSettings.dru_chat.new_msg_url
    const currentId = drupalSettings.dru_chat.current_id
    const pusher_configs = {
      app_key: drupalSettings.dru_chat.pusher_app_key,
      cluster: drupalSettings.dru_chat.pusher_cluster,
      presence_url: drupalSettings.dru_chat.presence_url,
    }

    // Initialize chat app
    new Chat(messages_url, new_message_url, pusher_configs, currentId);

  })

})(jQuery, Drupal, drupalSettings)






